from library import Song

class Player:
    def __init__(self):
        self.song = None
        self.playing = False

    def play(self, song:Song):
        self.song = song
        self.playing = True
        self.position = 0

    def pause(self):
        self.playing = False

    def update(self, delta):
        if self.playing:
            self.position += delta
            if self.position > self.song.length:
                self.position = self.song.length
                self.playing = False

player = Player()