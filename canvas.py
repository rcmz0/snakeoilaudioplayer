import canvasfunctions

def RGB888toRGB565(r, g, b):
    return ((g & 0x1c) << 11) | ((b & 0xf8) << 5) | (r & 0xf8) | (g >> 5)

class Canvas:
    black = RGB888toRGB565(0x00, 0x00, 0x00)
    mgray = RGB888toRGB565(0x7f, 0x7f, 0x7f)
    white = RGB888toRGB565(0xff, 0xff, 0xff)

    def __init__(self, *args):
        if len(args) == 1:
            canvasfunctions.load(self, args[0])
            self.path = args[0]
        elif len(args) == 2:
            self.width = args[0]
            self.height = args[1]
            self.buffer = bytearray(self.width*self.height*2)
            self.path = None

    # def copy(self):
    #     other = Canvas(self.width, self.height)
    #     other.set(self)
    #     return other
    
    def clear(self):
        canvasfunctions.clear(self)
        return self

    def set(self, other):
        canvasfunctions.set(self, other)
        return self

    def add(self, other):
        canvasfunctions.add(self, other)
        return self

    def luminance(self, l):
        canvasfunctions.luminance(self, l)
        return self

    def draw_rect(self, x, y, w, h, color):
        canvasfunctions.draw_rect(self, x, y, w, h, color)
        return self

    def invert_rect(self, x, y, w, h):
        canvasfunctions.invert_rect(self, x, y, w, h)
        return self

    def draw_text(self, x, y, text, font, color):
        canvasfunctions.draw_text(self, x, y, text, font, color)
        return self

    def draw_canvas(self, x, y, other):
        canvasfunctions.draw_canvas(self, x, y, other)
        return self
