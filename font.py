class Glyph:
    '''
    width
    height
    left
    top
    advance
    bitmap
    '''

class Font:
    nbcodes = 2**16 # we store the hole basic multilingual plane
    intwidth = 4 # for file io
    byteorder = 'little' # for file io

    @classmethod
    def generate(cls, inpath, height, outpath):
        import freetype

        face = freetype.Face(inpath)
        
        if face.has_fixed_sizes: # bitmap fonts
            heights = [s.height for s in face.available_sizes]
            if height in heights:
                face.select_size(heights.index(height))
            else:
                raise ValueError()
            ascend = height
        else: # vector fonts
            face.set_char_size(0, round(height*face.units_per_EM/face.height*64*72), 0, 1) # correct the height
            ascend = round(height*face.ascender/face.height)

        biggestglyph = (0, 0, 0, 0)
        longestglyph = (0, 0, 0, 0)
        for code in range(cls.nbcodes): # find the biggest glyph area
            face.load_char(code)
            w, h = face.glyph.bitmap.width, face.glyph.bitmap.rows
            t = (chr(code), w, h, w*h)
            if t[3] > biggestglyph[3]:
                biggestglyph = t
            if max(t[1], t[2]) > max(longestglyph[1], longestglyph[2]):
                longestglyph = t
        print('biggest glyph :', biggestglyph, 'longest glyph :', longestglyph)

        stride = biggestglyph[3] + 5 # distance between to glyphs in the file
        data = bytearray(cls.nbcodes * stride) # store the glyphs data

        for code in range(cls.nbcodes):
            face.load_char(code)
            glyph = face.glyph
            offset = code * stride
            data[offset+0] = glyph.bitmap.width
            data[offset+1] = glyph.bitmap.rows
            data[offset+2] = glyph.bitmap_left+128 # convert signed int to unsigned int 
            data[offset+3] = ascend-glyph.bitmap_top+128 # more usefull, we dont care about baselines
            data[offset+4] = round(glyph.advance.x/64)
            data[offset+5 : offset+5+glyph.bitmap.width*glyph.bitmap.rows] = glyph.bitmap.buffer # much faster but glitchs with unifont
            # for i in range(glyph.bitmap.width*glyph.bitmap.rows):
            #     data[offset+5+i] = glyph.bitmap.buffer[i]

        with open(outpath, 'wb') as f:
            f.write(height.to_bytes(cls.intwidth, cls.byteorder))
            f.write(stride.to_bytes(cls.intwidth, cls.byteorder))
            f.write(data)

    def __init__(self, path, cachelen=256):
        self.path = path
        self.cachelen = cachelen
        self.file = open(self.path, 'rb')
        self.height = int.from_bytes(self.file.read(self.intwidth), self.byteorder)
        self.stride = int.from_bytes(self.file.read(self.intwidth), self.byteorder)
        self.cache = {}

    def get_glyph(self, code):
        try:
            glyph = self.cache[code]
        except KeyError:
            glyph = Glyph()
            try:
                # print('font cache miss', self.path, code, chr(code))
                self.file.seek(self.intwidth*2 + code*self.stride)
                data = self.file.read(self.stride)
                glyph.width = data[0]
                glyph.height = data[1]
                glyph.left = data[2]-128
                glyph.top = data[3]-128
                glyph.advance = data[4]
                glyph.bitmap = data[5 : 5+glyph.width*glyph.height]
                self.cache[code] = glyph
            except Exception as e:
                print(e, code)
                glyph.width = 0
                glyph.height = 0
                glyph.left = 0
                glyph.top = 0
                glyph.advance = 0
                glyph.bitmap = bytes()
        return glyph

    def print_string(self, text):
        '''
        Print string in ascii art
        '''

        r = ''
        shades = ' ░▒▓█'
        buffer = [[0 for x in range(100)] for y in range(self.height+2)]

        advance = 0
        for c in text:
            glyph = self.get_glyph(ord(c))
            for y in range(glyph.height):
                for x in range(glyph.width):
                    px = advance + glyph.left + x
                    py = 1 + glyph.top + y
                    if px >= 0 and px < len(buffer[0]) and py >= 0 and py < len(buffer):
                        buffer[py][px] += glyph.bitmap[y * glyph.width + x]
            advance += glyph.advance

        for row in buffer:
            for val in row:
                r += shades[min(round(val/255*(len(shades)-1)), len(shades)-1)]
            r += '\n'

        print(r)

'''
def draw_string(img, font, x, y, string):
    if font is None:
        img.draw_string(x, y+4, string, mono_space=False)
    else:
        advance = 0
        for c in string:
            glyph = font.get_glyph(ord(c))
            width = glyph.width
            height = glyph.height
            left = glyph.left
            top = glyph.top
            for gx in range(width):
                px = x + advance + left + gx
                for gy in range(height):
                    py = y + top + gy
                    v = glyph.bitmap[gy * width + gx]
                    img.set_pixel(px, py, utils.gray565(v))
            advance += glyph.advance
'''

if __name__ == '__main__':
    from sys import argv
    Font.generate(argv[1], int(argv[2]), argv[3])
    font = Font(argv[3])
    font.print_string('A Aj안✃▒')
