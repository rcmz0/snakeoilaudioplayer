import os
import math

def avg(l):
    return sum(l) / len(l)

def clamp(v, _min, _max):
    return min(max(_min, v), _max)

def packbits(inbytes, depth):
    '''
    [0bXYZ00000, 0bUVW00000], 3 -> [0bXYZUVW00]
    '''
    outbytes = bytearray()
    outbyte = 0
    bit_index = 0
    for inbyte in inbytes:
        for d in range(depth):
            bit = (inbyte >> (7 - d)) & 1
            outbyte |= bit << (7 - bit_index)
            bit_index += 1
            if bit_index == 8:
                outbytes.append(outbyte)
                outbyte = 0
                bit_index = 0
    if bit_index != 0:
        outbytes.append(outbyte)
    return outbytes

def unpackbitsinto(inbytes, depth, outbytes):
    '''
    [0bXYZUVW00], 3 -> [0bXYZ00000, 0bUVW00000]
    '''
    # optimized versions
    if depth == 8:
        for i, inbyte in enumerate(inbytes):
            outbytes[i] = inbyte
    elif depth == 1:
        for i, inbyte in enumerate(inbytes):
            for d in range(8):
                outbytes[i * 8 + d] = (inbyte >> (7 - d) & 1) << 7
    else:
        raise NotImplementedError()

def unpackbits(inbytes, depth):
    '''
    [0bXYZUVW00], 3 -> [0bXYZ00000, 0bUVW00000]
    '''
    # optimized versions
    if depth == 8:
        outbytes = inbytes
    elif depth == 1:
        outbytes = bytearray(len(inbytes) * 8)
        for i, inbyte in enumerate(inbytes):
            for d in range(8):
                outbytes[i * 8 + d] = (inbyte >> (7 - d) & 1) * 0xff
    # general version
    else:
        outbytes = bytearray()
        outbyte = 0
        bit_index = 0
        for inbyte in inbytes:
            for d in range(8):
                bit = (inbyte >> (7 - d)) & 1
                outbyte |= bit << (7 - bit_index)
                bit_index += 1
                if bit_index == depth:
                    outbytes.append(outbyte)
                    outbyte = 0
                    bit_index = 0
        if bit_index != 0:
            outbytes.append(outbyte)
    return outbytes

def packedsize(n, depth):
    '''
    number of bytes needed to store n unpacked bytes
    '''
    d, m = divmod(n * 8, depth)
    return d + (m > 0)

def unpackedsize(n, depth):
    '''
    number of bytes needed to store n packed bytes
    '''
    d, m = divmod(n * depth, 8)
    return d + (m > 0)

def color565(r, g, b):
    '''
    Return RGB565 color value.

    Args:
        r (int): Red value.
        g (int): Green value.
        b (int): Blue value.
    '''
    return (r & 0xf8) << 8 | (g & 0xfc) << 3 | b >> 3

def gray565(v):
    '''
    Return RGB565 color value

    Args:
        v : grayscale value

    Notes:
        bitshifting gives the same result as proper float rounding +- 1
        remember to reverse the byte order !
        image.grayscale_to_rgb(v) -> (v, v, v)
        the r825 | g826 tables are just round(x/255*31) | round(x/255*63)
    '''
    # v5 = v >> 3
    # v6 = v >> 2
    # return (v5 << 3) | (v6 >> 3) | ((v6 & 0x7) << 13) | (v5 << 8)
    return (v & 0xf8) | (v >> 5) | ((v & 0x1c) << 11) | ((v & 0xf8) << 5) # optimized version

def isdir(path:str):
    return bool(os.stat(path)[0] & (1 << 14))

def isfile(path:str):
    return bool(os.stat(path)[0] & (1 << 15))

def hasextension(path:str, extensions):
    extension = path.rpartition('.')[2].lower()
    if isinstance(extensions, str):
        return extension == extensions
    else:
        return extension in extensions
