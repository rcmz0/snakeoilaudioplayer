from view import View
from tween import Tween
from canvas import Canvas
from utils import clamp

class ListView(View):
    entry_font = View.small_font
    title_font = View.big_font
    tween_speed = 10
    x_margin = 1

    def __init__(self, title, entries, background_path=None):
        self.title = title
        self.entries = entries
        self.set_background(background_path)

        self.len = 0
        self.top = 0
        self.selector = 0
        self.top_tween = Tween(self.tween_speed, self.entry_font.height)
        self.selector_tween = Tween(self.tween_speed, self.entry_font.height)

    def draw(self, canvas:Canvas, delta):
        # compute how many entries can fit on screen
        self.len = (canvas.height - self.title_font.height) // self.entry_font.height

        # entries
        self.top_tween.update(delta)
        for i in range(max(self.top - 1, 0), min(self.top + self.len + 1, len(self.entries))):
            entry_y = self.title_font.height + (i - self.top)*self.entry_font.height + int(self.top_tween())
            canvas.draw_text(self.x_margin, entry_y, str(self.entries[i]), self.entry_font, canvas.white)

        # title
        canvas.draw_rect(0, 0, canvas.width, self.title_font.height, canvas.black)
        canvas.draw_text(self.x_margin, 0, self.title, self.title_font, canvas.white)

        # selector
        self.selector_tween.update(delta)
        highlight_y = self.title_font.height + (self.selector - self.top)*self.entry_font.height + int(self.top_tween() + self.selector_tween())
        canvas.invert_rect(0, highlight_y, canvas.width, self.entry_font.height)

        # background
        self.draw_background(canvas)

    def on_scroll(self, scroll:int):
        previous_selector = self.selector
        self.selector = clamp(self.selector+scroll, 0, len(self.entries)-1)
        self.selector_tween.start(previous_selector-self.selector, 0)

        previous_top = self.top
        if self.selector < self.top:
            self.top = self.selector
        if self.selector > self.top+self.len-1:
            self.top = self.selector-self.len+1
        self.top_tween.start(self.top-previous_top, 0)
