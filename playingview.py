from view import View
from canvas import Canvas
from player import player

def timetext(v):
    return '%02d:%02d' % (v//60, v%60)

class PlayingView(View):
    bar_x_margin = 1
    bar_y_margin = 1
    bar_thickness = 4

    def draw(self, canvas:Canvas, delta):
        if player.song is None:
            canvas.draw_text(0, 0, '(wind sound ...)', self.small_font, canvas.white)
        else:
            # song information
            canvas.draw_text(0, self.small_font.height*0, player.song.name, self.small_font, canvas.white)
            canvas.draw_text(0, self.small_font.height*1, player.song.album.name, self.small_font, canvas.white)
            canvas.draw_text(0, self.small_font.height*2, player.song.artist.name, self.small_font, canvas.white)

            # bar
            barx = self.bar_x_margin
            bary = canvas.height - self.bar_y_margin - self.bar_thickness
            barw = canvas.width - self.bar_x_margin*2
            barh = self.bar_thickness
            canvas.draw_rect(barx, bary, barw, barh, canvas.mgray)
            canvas.draw_rect(barx, bary, int(barw*(player.position/player.song.length)), barh, canvas.white)

            # times
            canvas.draw_text(barx, bary-self.small_font.height, timetext(player.position), self.small_font, canvas.white)
            canvas.draw_text(barx+barw-28, bary-self.small_font.height, timetext(player.song.length), self.small_font, canvas.white)

            # background
            self.set_background(player.song.art)
            self.draw_background(canvas)

    def on_back(self):
        self.manager.pop()

    def on_playing(self):
        self.on_back()
