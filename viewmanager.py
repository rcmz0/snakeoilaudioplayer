from view import View
from tween import Tween
from canvas import Canvas

class ViewManager:
    tween_speed = 5

    def __init__(self, width, height):
        self.stack = []
        self.canvas = Canvas(width, height)
        self.top_canvas = Canvas(width, height)
        self.anim_canvas = Canvas(width, height)
        self.tween = Tween(self.tween_speed, height)

    def top(self) -> View:
        return self.stack[-1]

    def push(self, view:View):
        view.manager = self
        self.stack.append(view)
        self.anim_canvas.set(self.top_canvas)
        self.canvases_order = [self.anim_canvas, self.top_canvas]
        self.tween.start(1, 0)

    def pop(self):
        if len(self.stack) > 1:
            self.stack.pop()
            self.anim_canvas.set(self.top_canvas)
            self.canvases_order = [self.top_canvas, self.anim_canvas]
            self.tween.start(0, 1)

    def draw(self, delta):
        self.top_canvas.clear()
        self.top().draw(self.top_canvas, delta)
        self.tween.update(delta)
        self.canvas.clear() # necessary ?
        self.canvas.draw_canvas(0, 0, self.canvases_order[0])
        self.canvas.draw_canvas(0, int(self.tween()), self.canvases_order[1])
        return self.canvas.buffer

    def on_scroll(self, scroll:int):
        self.top().on_scroll(scroll)

    def on_select(self):
        self.top().on_select()

    def on_back(self):
        self.top().on_back()

    def on_playing(self):
        self.top().on_playing()

    def on_option(self):
        self.top().on_option()
