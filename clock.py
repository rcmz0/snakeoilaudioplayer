import time
from utils import avg
from array import array

class Clock:
    def __init__(self, nb_deltas):
        self.t = 0
        self.buf_index = 0
        self.delta_buf = array('f', 1 for _ in range(nb_deltas))

    def tick(self):
        self.previous_t = self.t
        self.t = time.ticks_ms()
        self.buf_index = (self.buf_index + 1) % len(self.delta_buf)
        self.delta_buf[self.buf_index] = (self.t - self.previous_t) / 1000

    def delta(self):
        return self.delta_buf[self.buf_index]

    def fps(self):
        return int(1/self.delta())

    def deltas(self):
        return (self.delta_buf[self.buf_index], min(self.delta_buf), avg(self.delta_buf), max(self.delta_buf))

    def fpss(self):
        ds = self.deltas()
        return (int(1/ds[0]), int(1/ds[3]), int(1/ds[2]), int(1/ds[1]))
