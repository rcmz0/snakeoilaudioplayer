class Tween:
    def __init__(self, speed=1, factor=1):
        self.speed = speed
        self.factor = factor
        self.value = 0
        self.goal = 0

    def set(self, value):
        self.value = value

    def start(self, f, t):
        self.value = f
        self.goal = t

    def update(self, delta):
        if self.value < self.goal:
            self.value = min(self.value+delta*self.speed, self.goal)
        elif self.value > self.goal:
            self.value = max(self.value-delta*self.speed, self.goal)

    def __call__(self):
        return self.value*self.factor
    