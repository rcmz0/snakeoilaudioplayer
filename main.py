from fpioa_manager import fm
from Maix import GPIO
from machine import SPI
import sys
from clock import Clock
from ssd1351 import Display
from buttons import Button, FakeRotary
from viewmanager import ViewManager
from libraryviews import ArtistsView
from player import player

'''
Notes:
avoid using pins 4-5 25-29
'''

fm.register(31, fm.fpioa.SPI0_SCLK) # sclk
fm.register(32, fm.fpioa.SPI0_D0) # mosi
fm.register(33, fm.fpioa.GPIOHS11) # cs
fm.register(34, fm.fpioa.GPIOHS12) # dc
fm.register(35, fm.fpioa.GPIOHS13) # rst
cs_pin = GPIO(GPIO.GPIOHS11)
dc_pin = GPIO(GPIO.GPIOHS12)
rst_pin = GPIO(GPIO.GPIOHS13)
spi = SPI(SPI.SPI0, baudrate=18000000)
display = Display(spi, cs_pin, dc_pin, rst_pin)

# buttons
fm.register(0, fm.fpioa.GPIOHS20)
fm.register(1, fm.fpioa.GPIOHS21)
fm.register(2, fm.fpioa.GPIOHS22)
fm.register(3, fm.fpioa.GPIOHS23)
select_btn = Button(20)
back_btn = Button(21)
playing_btn = Button(22)
option_btn = Button(23)

# rotary
fm.register(6, fm.fpioa.GPIOHS24)
fm.register(7, fm.fpioa.GPIOHS25)
rotary = FakeRotary(24, 25)

clock = Clock(100)
viewmanager = ViewManager(display.width, display.height)
viewmanager.push(ArtistsView())

while True:
    clock.tick()
    delta = clock.delta()
    print('\r' + str(clock.fpss()) + '\t', end='')

    player.update(delta)
    rotary.update(delta)
    select_btn.update()
    back_btn.update()
    playing_btn.update()
    option_btn.update()

    scroll = rotary.scroll
    if scroll != 0:
        viewmanager.on_scroll(scroll)
        # print('scroll', scroll)

    if select_btn.justpressed:
        viewmanager.on_select()
        # print('select')

    if back_btn.justpressed:
        viewmanager.on_back()
        # print('back')

    if playing_btn.justpressed:
        viewmanager.on_playing()
        # print('playing')

    if option_btn.justpressed:
        viewmanager.on_option()
        # print('option')

    display.draw(viewmanager.draw(delta))
