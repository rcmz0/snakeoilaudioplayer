from listview import ListView
from playingview import PlayingView
from library import Song, Album, Artist, library
from player import player

class LibraryView(ListView):
    def push(self, view):
        self.manager.push(view)

    def on_back(self):
        self.manager.pop()

    def on_playing(self):
        self.manager.push(PlayingView())

class ArtistsView(LibraryView):
    def __init__(self):
        title = 'Artists'
        entries = library.artists
        super().__init__(title, ['All']+entries)

    def on_select(self):
        if len(self.entries) > 0:
            entry = self.entries[self.selector]
            if entry == 'All':
                self.push(AlbumsView())
            else:
                self.push(AlbumsView(entry))

class AlbumsView(LibraryView):
    def __init__(self, arg=None):
        if isinstance(arg, Artist):
            self.artist = arg
            title = arg.name
            entries = arg.albums
            background = arg.art
        else:
            title = 'Albums'
            entries = library.albums
            background = None
        super().__init__(title, ['All']+entries, background)

    def on_select(self):
        if len(self.entries) > 0:
            entry = self.entries[self.selector]
            if entry == 'All':
                if hasattr(self, 'artist'):
                    self.push(SongsView(self.artist))
                else:
                    self.push(SongsView())
            else:
                self.push(SongsView(entry))

class SongsView(LibraryView):
    def __init__(self, arg=None):
        if isinstance(arg, Album):
            self.album = arg
            title = arg.name
            entries = arg.songs
            background = arg.art
        elif isinstance(arg, Artist):
            self.artist = arg
            title = arg.name
            entries = arg.songs
            background = arg.art
        else:
            title = 'Songs'
            entries = library.songs
            background = None
        super().__init__(title, entries, background)

    def on_select(self):
        if len(self.entries) > 0:
            entry = self.entries[self.selector]
            player.play(entry)
